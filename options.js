document.addEventListener('DOMContentLoaded', function () {
    let btn = document.querySelector('#save_setting');
    let auto = document.querySelector('#auto_transform');
    let list = document.querySelector('#special_list');

    chrome.storage.local.get(['auto_transform', 'special_list', 'special_rule'], function (res) {
        auto.checked = res.auto_transform ? true : false;
        list.value = res.special_list ? res.special_list : '';
        let rule = res.special_rule == 1 ? 1 : 0;
        document.querySelector('.radio[value="' + rule + '"]').checked = true;
    });

    btn.onclick = function () {
        let rule = document.querySelector('.radio:checked').value == '1' ? 1 : 0;
        chrome.storage.local.set({
            auto_transform: auto.checked,
            special_list: list.value,
            special_rule: rule
        }, function () {
            let tips = document.querySelector('.tips');
            tips.style.display = 'inline-block';
            setTimeout(function() {
                tips.style.display = 'none';
            }, 1500);
        });
    };

    document.querySelectorAll('[data-i18n]').forEach(function (ele) {
        let name = ele.getAttribute('data-i18n');
        let message = chrome.i18n.getMessage(name);
        ele.innerText = message;
    });

    document.querySelectorAll('[data-i18n-placeholder]').forEach(function (ele) {
        let name = ele.getAttribute('data-i18n-placeholder');
        let message = chrome.i18n.getMessage(name);
        ele.placeholder = message;
    });

    if (!/^zh-?/.test(chrome.i18n.getUILanguage())) {
        document.body.className = 'en';
    }
});